import React, { Fragment, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Producto from './components/Producto';
import Carrito from './components/Carrito';
import Footer from './components/Footer';

function App() {

  //Listado de productos
  const [ productos, guardarProductos ] = useState([
    { id: 1, nombre : 'Caguamita', precio : 32 },
    { id: 2, nombre : 'Tequilita', precio : 100 },
    { id: 3, nombre : 'Roncito', precio : 100 },
    { id: 4, nombre : 'Whiskito', precio : 90 }
  ]);

  //State para un carrito de compras
  const [ carrito, agregarProducto ] = useState([ 

   ]);

  const fecha = new Date().getFullYear();

  return (
    //Con el Fragment reemplazamos el div
    <Fragment>
      <Header
        titulo = 'Tienda virtual'
      />

      <h1>Listado de productos</h1>
      { 
        productos.map(producto => (
          <Producto
            key             = {producto.id}
            producto        = {producto}
            productos       = {productos}
            carrito         = {carrito}
            agregarProducto = {agregarProducto}
          />
        )) 
      }
      <Carrito
        carrito         = {carrito}
        agregarProducto = {agregarProducto}
      />
      <Footer fecha = { fecha }/>
    </Fragment>
  );

}

export default App;

{/* <header className="App-header">
  <img src={logo} className="App-logo" alt="logo" />
  <p>
    Edit <code>src/App.js</code> and save to reload.
  </p>
  <a
    className="App-link"
    href="https://reactjs.org"
    target="_blank"
    rel="noopener noreferrer"
  >
    Ando aprendiendo react
  </a>
</header> */}