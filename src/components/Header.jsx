import React from 'react';

function Header({ titulo }){

    const edad = 19;
    
    let mensaje;

    if(edad >= 18)
        mensaje = 'Eres mayor de edad';
    else
        mensaje = 'Eres menor de edad';
    
    return (
        <h1 id="encabezado" className="encabezado"> { titulo } { mensaje } </h1>
    );

}

export default Header;